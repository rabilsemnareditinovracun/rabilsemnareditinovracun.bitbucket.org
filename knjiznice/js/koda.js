const baseUrl = 'https://rest.ehrscape.com/rest/v1';
const queryUrl = baseUrl + '/query';

const username = "ois.seminar";
const password = "ois4fri";

const pac1 = {
    ime: "Cvetka",
    priimek: "Božič",
    rojen: "1984-07-26",
    spol: "FEMALE"
};
const pac2 = {
    ime: "Petra",
    priimek: "Rozman",
    rojen: "1992-03-01",
    spol: "FEMALE"
};
const pac3 = {
    ime: "Jure",
    priimek: "Petrič",
    rojen: "1999-06-06",
    spol: "MALE"
};

let meritve = {};
let party = {};

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
    return "Basic " + btoa(username + ":" + password);
}

function dodajZapis(ehrId, visina, teza, datum, callback) {
    const parametriZahteve = {
        ehrId: ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT'
    };
    const hour = Math.round((Math.random() * 23) % 23);
    const h = hour < 10 ? "0" + hour : hour;

    const min = Math.round((Math.random() * 59) % 59);
    const m = min < 10 ? "0" + min : min;

    const sec = Math.round((Math.random() * 59) % 59);
    const s = sec < 10 ? "0" + sec : sec;
    const podatki = {
        "ctx/language": "en",
        "ctx/territory": "SI",
        "ctx/time": datum + "T" + h + ":" + m + ":" + s,
        "vital_signs/height_length/any_event:0/body_height_length": visina,
        "vital_signs/body_weight/any_event:0/body_weight": teza,
    };
    $.ajax({
        url: baseUrl + "/composition?" + $.param(parametriZahteve),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(podatki),
        headers: {
            "Authorization": getAuthorization()
        },
        success: function (res) {
            callback();
        },
        error: function (err) {
            document.getElementById('error').innerText = JSON.parse(err.responseText).userMessage;
        }
    });
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @param callback
 */
function generirajPodatke(stPacienta, callback) {
    document.getElementById('error').innerText = "";
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        headers: {
            "Authorization": getAuthorization()
        },
        success: function (data) {
            const ehrId = data.ehrId;
            switch (stPacienta) {
                case 1: {
                    pac = pac1;
                    break;
                }
                case 2: {
                    pac = pac2;
                    break;
                }
                case 3: {
                    pac = pac3;
                    break;
                }
            }
            const party = {
                firstNames: pac.ime,
                lastNames: pac.priimek,
                dateOfBirth: pac.rojen,
                gender: pac.spol,
                additionalInfo: {"ehrId": ehrId}
            };
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                headers: {
                    "Authorization": getAuthorization()
                },
                contentType: 'application/json',
                data: JSON.stringify(party),
                success: function (party) {
                    if (party.action === 'CREATE') {
                        switch (stPacienta) {
                            case 1: {
                                dodajZapis(ehrId, 170, 62, "2000-01-01", function () {
                                    dodajZapis(ehrId, 171, 65, "2002-02-02", function () {
                                        dodajZapis(ehrId, 170, 64, "2004-03-03", function () {
                                            dodajZapis(ehrId, 168, 66, "2006-04-04", function () {
                                                dodajZapis(ehrId, 169, 67, "2008-05-05", function () {
                                                    dodajZapis(ehrId, 170, 70, "2010-06-06", function () {
                                                        dodajZapis(ehrId, 169, 65, "2012-07-07", function () {
                                                            dodajZapis(ehrId, 171, 63, "2014-08-08", function () {
                                                                dodajZapis(ehrId, 170, 62, "2016-09-09", function () {
                                                                    dodajZapis(ehrId, 169, 63, "2018-10-10", function () {
                                                                        callback(ehrId);
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                                break;
                            }
                            case 2: {
                                dodajZapis(ehrId, 127, 40, "2000-01-01", function () {
                                    dodajZapis(ehrId, 135, 48, "2002-02-02", function () {
                                        dodajZapis(ehrId, 141, 54, "2004-03-03", function () {
                                            dodajZapis(ehrId, 147, 61, "2006-04-04", function () {
                                                dodajZapis(ehrId, 153, 68, "2008-05-05", function () {
                                                    dodajZapis(ehrId, 157, 75, "2010-06-06", function () {
                                                        dodajZapis(ehrId, 160, 80, "2012-07-07", function () {
                                                            dodajZapis(ehrId, 162, 84, "2014-08-08", function () {
                                                                dodajZapis(ehrId, 161, 88, "2016-09-09", function () {
                                                                    dodajZapis(ehrId, 162, 93, "2018-10-10", function () {
                                                                        callback(ehrId);
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                                break;
                            }
                            case 3: {
                                dodajZapis(ehrId, 72, 11, "2000-01-01", function () {
                                    dodajZapis(ehrId, 94, 16, "2002-02-02", function () {
                                        dodajZapis(ehrId, 107, 20, "2004-03-03", function () {
                                            dodajZapis(ehrId, 121, 25, "2006-04-04", function () {
                                                dodajZapis(ehrId, 139, 30, "2008-05-05", function () {
                                                    dodajZapis(ehrId, 157, 38, "2010-06-06", function () {
                                                        dodajZapis(ehrId, 166, 43, "2012-07-07", function () {
                                                            dodajZapis(ehrId, 175, 48, "2014-08-08", function () {
                                                                dodajZapis(ehrId, 182, 51, "2016-09-09", function () {
                                                                    dodajZapis(ehrId, 185, 53, "2018-10-10", function () {
                                                                        callback(ehrId);
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                                break;
                            }
                        }
                    }
                },
                error: function (err) {
                    document.getElementById('error').innerText = JSON.parse(err.responseText).userMessage;
                }
            });
        },
        error: function (err) {
            document.getElementById('error').innerText = JSON.parse(err.responseText).userMessage;
        }
    });
}

function generirajVsePodatke() {
    document.getElementById('msg').innerHTML = "Generiranje podatkov... <div class='loader'></div>";
    generirajPodatke(1, function (ehr1) {
        generirajPodatke(2, function (ehr2) {
            generirajPodatke(3, function (ehr3) {
                const p1 = "<option value='" + ehr1 + "'>" + pac1.ime + " " + pac1.priimek + "</option>";
                const p2 = "<option value='" + ehr2 + "'>" + pac2.ime + " " + pac2.priimek + "</option>";
                const p3 = "<option value='" + ehr3 + "'>" + pac3.ime + " " + pac3.priimek + "</option>";

                const p1i = pac1.ime + " " + pac1.priimek + " (" + ehr1 + ")<br>";
                const p2i = pac2.ime + " " + pac2.priimek + " (" + ehr2 + ")<br>";
                const p3i = pac3.ime + " " + pac3.priimek + " (" + ehr3 + ")<br>";

                document.getElementById('ehr-id').value = ehr1;
                preberiPodatkeZaEhrId();
                document.getElementById('msg').innerHTML = "Pacienti generirani:<br>" + p1i + p2i + p3i;
                document.getElementById('izbira').innerHTML = p1 + p2 + p3;
            });
        });
    });
}

const regex = new RegExp("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}");

function preberiPodatkeZaEhrId() {
    const ehr = document.getElementById('ehr-id').value.toLowerCase();
    if (!regex.test(ehr)) {
        document.getElementById('error').innerText = "Neveljaven EHR ID! (Preverite obliko)";
        return;
    }
    document.getElementById('pacient').innerHTML = "Nalaganje podatkov... <div class='loader'></div>";
    document.getElementById("meritev-ime").innerText = "Ni izbrana";
    document.getElementById("teza").innerText = "--";
    document.getElementById("teza-opis").innerText = "--";
    document.getElementById("visina").innerText = "--";
    document.getElementById("visina-opis").innerText = "--";
    document.getElementById("bmi").innerText = "--";
    document.getElementById("bmi-stanje").innerText = "--";
    document.getElementById("bmi-opis").innerText = "--";
    document.getElementById("bmr").innerText = "--";
    document.getElementById("bmr-opis").innerText = "--";
    document.getElementById('error').innerText = "";
    party = {};
    $.ajax({
        url: baseUrl + "/demographics/ehr/" + ehr + "/party",
        type: 'GET',
        headers: {
            "Authorization": getAuthorization()
        },
        success: function (data) {
            party = data.party;
            const birth = new Date(party.dateOfBirth + "T00:00:00");
            let age = new Date().getFullYear() - birth.getFullYear();
            const mes = new Date().getMonth() - birth.getMonth();
            if (mes < 0 || (mes === 0 && new Date().getDate() < birth.getDate())) age--;
            document.getElementById('pacient').innerText = party.firstNames + " " + party.lastNames
                + " (" + party.gender + ") - " + party.dateOfBirth + " (" + age + " let)";


            meritve = {};
            $.ajax({
                url: baseUrl + "/view/" + ehr + "/weight",
                type: 'GET',
                headers: {
                    "Authorization": getAuthorization()
                },
                success: function (data) {
                    for (let i = 0; i < data.length; i++) {
                        meritve[data[i].time] = {
                            teza: data[i].weight,
                            visina: 0
                        };
                    }
                    $.ajax({
                        url: baseUrl + "/view/" + ehr + "/height",
                        type: 'GET',
                        headers: {
                            "Authorization": getAuthorization()
                        },
                        success: function (data) {
                            for (let j = 0; j < data.length; j++) {
                                if (meritve.hasOwnProperty(data[j].time)) {
                                    meritve[data[j].time].visina = data[j].height;
                                } else {
                                    meritve[data[j].time] = {
                                        teza: 0,
                                        visina: data[j].height
                                    };
                                }
                            }
                            let sel = "";
                            for (m in meritve) {
                                sel = sel + "<option>" + m + "</option>";
                            }
                            document.getElementById('datumi').innerHTML = sel;
                            izrisiGrafe();
                        },
                        error: function (err) {
                            document.getElementById('error').innerText = JSON.parse(err.responseText).userMessage;
                        }
                    });
                },
                error: function (err) {
                    document.getElementById('error').innerText = JSON.parse(err.responseText).userMessage;
                }
            });
        },
        error: function (err) {
            document.getElementById('error').innerText = JSON.parse(err.responseText).userMessage;
        }
    });
}

function prikaziMeritev() {
    document.getElementById("meritev-ime").innerHTML = "Nalaganje...  <div class='loader'></div>";
    document.getElementById("teza").innerText = "--";
    document.getElementById("teza-opis").innerText = "--";
    document.getElementById("visina").innerText = "--";
    document.getElementById("visina-opis").innerText = "--";
    document.getElementById("bmi").innerText = "--";
    document.getElementById("bmi-stanje").innerText = "--";
    document.getElementById("bmi-opis").innerText = "--";
    document.getElementById("bmr").innerText = "--";
    document.getElementById("bmr-opis").innerText = "--";
    document.getElementById('error').innerText = "";

    const datum = document.getElementById("datumi").value;
    const m = meritve[datum];

    const teza = m.teza;
    const visina = m.visina;
    const spol = party.gender;
    const birth = new Date(party.dateOfBirth + "T00:00:00");
    let age = new Date().getFullYear() - birth.getFullYear();
    const mes = new Date().getMonth() - birth.getMonth();
    if (mes < 0 || (mes === 0 && new Date().getDate() < birth.getDate())) age--;

    const tezab = teza !== 0;
    if (tezab) {
        document.getElementById("teza").innerText = teza;
        document.getElementById("teza-opis").innerText = "Izmerjena teža.";
    } else {
        document.getElementById("teza-opis").innerText = "Ni podatka o teži za to meritev.";

    }
    const visinab = teza !== 0;
    if (visinab) {
        document.getElementById("visina").innerText = visina;
        document.getElementById("visina-opis").innerText = "Izmerjena višina.";
    } else {
        document.getElementById("visina-opis").innerText = "Ni podatka o teži za to meritev.";

    }
    const param = {
        height: visina / 100,
        weight: teza,
        age: age,
        male: spol === "MALE"
    };
    if (tezab && visinab) {
        $.ajax({
            url: "https://redepicness.ovh/health.php?" + $.param(param),
            type: 'GET',
            success: function (data) {
                document.getElementById("meritev-ime").innerText = datum;
                const bmi = Math.round(data.bmi * 100) / 100;
                document.getElementById("bmi").innerText = bmi;
                let stanje = "";
                if (bmi <= 16.0) {
                    stanje = "Huda nedohranjenost";
                } else if (bmi <= 17.0) {
                    stanje = "Zmerna nedohranjenost";
                } else if (bmi <= 18.5) {
                    stanje = "Blaga nedohranjenost";
                } else if (bmi <= 25.0) {
                    stanje = "Normalna telesna masa";
                } else if (bmi <= 30.0) {
                    stanje = "Zvečana telesna masa (preadipoznost)";
                } else if (bmi <= 35.0) {
                    stanje = "Debelost stopnje I";
                } else if (bmi <= 40.0) {
                    stanje = "Debelost stopnje II";
                } else {
                    stanje = "Debelost stopnje III";
                }
                document.getElementById("bmi-stanje").innerText = stanje;
                document.getElementById("bmi-opis").innerHTML = "<strong>B</strong>ody <strong>M</strong>ass <strong>I</strong>ndex " +
                    "(<strong>I</strong>ndeks <strong>T</strong>elesne <strong>M</strong>ase) je antropološka mera," +
                    " definirana kot telesna masa (kg), deljena s kvadratom telesne višine (m). " +
                    "Pri primerno prehranjenem odraslem znaša med 21,4 in 25,6, pod 21,4 pomeni podhranjenost," +
                    " nad 25,6 pa prenahranjenost.";

                document.getElementById("bmr").innerText = Math.round(data.bmr * 100) / 100;
                document.getElementById("bmr-opis").innerHTML = "<strong>B</strong>asal <strong>M</strong>etabolic <strong>R</strong>ate " +
                    " (Bazalni metabolizem) je stopnja pretvorbe energije v organizmu, ki miruje, ima prazno prebavilo in ni pod temperaturnim stresom," +
                    " torej je temperatura njegovega okolja enaka njegovi normalni telesni temperaturi. V tem stanju porablja" +
                    " energijo samo za vzdrževanje osnovnih življenjskih funkcij.";
            },
            error: function (err) {
                document.getElementById("meritev-ime").innerText = datum;
                document.getElementById('error').innerText = "Napaka pri dostopu zunanjega API vmesnika!";
            }
        });
    } else {
        document.getElementById("bmi-opis").innerText = "Ni dovolj podatkov za izračun BMI.";
        document.getElementById("bmr-opis").innerText = "Ni dovolj podatkov za izračun BMR.";
    }

}

function izrisiGrafe() {
    const mer = [];
    const teze = [];
    const visine = [];
    const leta = [];
    const bmi = [];
    const bmr = [];
    for (let m in meritve) {
        const date = new Date(m);
        mer.push(date.getMonth() + " - " + date.getFullYear());
        teze.push(meritve[m].teza);
        visine.push(meritve[m].visina);
        const birth = new Date(party.dateOfBirth);
        let age = date.getFullYear() - birth.getFullYear();
        const mes = date.getMonth() - birth.getMonth();
        if (mes < 0 || (mes === 0 && date.getDate() < birth.getDate())) age--;
        leta.push(age);
    }
    const req = {
        male: party.gender === "MALE" ? 1 : 0,
        teze: teze,
        visine: visine,
        leta: leta
    };
    $.ajax({
        url: "https://redepicness.ovh/health-multi.php?json=" + JSON.stringify(req),
        type: 'GET',
        success: function (data) {
            for (let i = 0; i < data.length; i++) {
                bmi.push(Math.round(data[i].bmi * 100) / 100);
                bmr.push(Math.round(data[i].bmr * 100) / 100);
            }
            mer.reverse();
            teze.reverse();
            visine.reverse();
            leta.reverse();
            bmi.reverse();
            bmr.reverse();
            izrisiGraf("graf-teza", "Teža", mer, teze, "red");
            izrisiGraf("graf-visina", "Višina", mer, visine, "blue");
            izrisiGraf("graf-bmi", "BMI", mer, bmi, "green");
            izrisiGraf("graf-bmr", "BMR", mer, bmr, "orange");
        },
        error: function (err) {
            document.getElementById("meritev-ime").innerText = "";
            document.getElementById('error').innerText = "Napaka pri dostopu zunanjega API vmesnika!";
        }
    });
}

function izrisiGraf(id, name, mer, dat, color) {
    const trace1 = {
        x: mer,
        y: dat,
        mode: 'lines+markers',
        name: name,
        line: {
            color: color
        }
    };

    const data = [trace1];

    const layout = {
        title: name + ' skozi meritve',
        showlegend: true,
        width: 555,
        height: 400,
        xaxis: {
            showline: true,
            showgrid: true,
            showticklabels: true,
            linecolor: 'rgb(204,204,204)',
            linewidth: 2,
            autotick: false,
            ticks: 'outside',
            tickcolor: 'rgb(204,204,204)',
            tickwidth: 2,
            ticklen: 5,
            tickfont: {
                family: 'Arial',
                size: 12,
                color: 'rgb(82, 82, 82)'
            },
            type: "text"
        },
        yaxis: {
            showline: true,
            showgrid: true,
            showticklabels: true,
            linecolor: 'rgb(204,204,204)',
            linewidth: 2,
            autotick: true,
            ticks: 'outside',
            tickcolor: 'rgb(204,204,204)',
            tickwidth: 2,
            ticklen: 5,
            tickfont: {
                family: 'Arial',
                size: 12,
                color: 'rgb(82, 82, 82)'
            },
            type: "number"
        },
        autosize: false,
    };

    Plotly.newPlot(id, data, layout);
}


